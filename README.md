
# Bilan du projet de BDD ET Manuel d'utilisation de l'application Web de statistiques démographiques

Cette application Web vous permet d'explorer des statistiques démographiques sur les populations, les mariages, les naissances et les décès au niveau des communes, des départements et des régions en France. Vous pouvez visualiser des rapports, des graphiques et même modifier les données de population.

## Installation

1. Assurez-vous d'avoir Python installé sur votre système. Vous pouvez le télécharger depuis [python.org](https://www.python.org/downloads/).


2. Installez les dépendances Python à l'aide de pip (le gestionnaire de paquets de Python si ca ne marche pas vous pouvez crée un environnement python voir (`Création de l'environnement virtuel`) ) :

    ```bash
    pip install -r requirements.txt
    ```

3. Configurez les informations de connexion à la base de données PostgreSQL dans les fichier `bdwebflask/config.json` :

    ```python
    dbname="dbname",
    user="user",
    password="password",
    host="pgsql",
    ```
### Création de l'environnement virtuel

1. Créez un environnement virtuel Python :
   ```bash
   python -m venv <nom_de_l'environnement>
   ```

2. Activez l’environnement virtuel :
   - Sous Unix/Linux :
     ```bash
     source <nom_de_l'environnement>/bin/activate
     ```
   - Sous Windows :
     ```bash
     <nom_de_l'environnement>\Scripts\activate
     ```
## Utilisation de la creation / insertion des données 
### Création de la base de donnée
1. Création des tables `create.sql` :
    ```bash
    psql -h pgsql
    ```
    ```bash
    \i create.sql
    ```
2. Insertions des données `insert.py` :
    ```bash
    python insert.py  
    ```
3. Ajout des views et triggers `optimisation.sql` :
    ```bash
    psql -h pgsql
    ```
    ```bash
    \i optimisation.sql

    ```
4. Test des Triggers (ajout automatique de la population totale par année du departement et région)
    ```bash
    python trigger_population.py
    ```

5. Test du trigger qui modifie population_region et population_departement quand population_commune est modifié :
    ```bash
    psql -h pgsql
    ```
    ```bash
    \i update_population_triggers.sql

    ```
## Utilisation du site web

1. Lancez l'application en exécutant le fichier `bdwebflask/runserver.py` :

    ```bash
    python runserver.py
    ```

2. Ouvrez votre navigateur Web et accédez à l'URL suivante : [http://localhost:8085/](http://localhost:8085/)

3. Explorez les différentes fonctionnalités de l'application :
   - Visualisation des statistiques démographiques par région, département et commune.
   - Consultation des rapports de performances pour les différentes requêtes SQL.
   - Affichage de graphiques pour les populations, les mariages, les naissances et les décès.
   - Modification des données de population pour les communes, les départements et les régions.
   - Undo: flèche de gauche ou ctrl-z , Redo : flèche de droite ou ctrl-y 
## Test Des Isolations sur les transactions
1. Lancez le test en exécutant le fichier `test_isolation.py` :

    ```bash
    python test_isolation.py
    ```
CREATE OR REPLACE VIEW population_departement_view AS 
SELECT c.code_departement, p.annee, SUM(p.NOMBRE) AS population_totale 
FROM population_commune p
JOIN commune c ON c.code_commune = p.code_commune
GROUP BY c.code_departement,p.annee;

CREATE OR REPLACE VIEW naissance_departement_view AS 
SELECT c.code_departement, n.annee_debut,n.annee_fin, SUM(n.NOMBRE) AS naissance_totale 
FROM naissance_commune n
JOIN commune c ON c.code_commune = n.code_commune
GROUP BY c.code_departement,n.annee_debut,n.annee_fin;

CREATE OR REPLACE VIEW deces_departement_view AS 
SELECT c.code_departement, d.annee_debut,d.annee_fin, SUM(d.NOMBRE) AS deces_totale 
FROM deces_commune d
JOIN commune c ON c.code_commune = d.code_commune
GROUP BY c.code_departement,d.annee_debut,d.annee_fin;


CREATE OR REPLACE VIEW population_region_view AS
SELECT r.code_region, p.annee, SUM(p.nombre) AS population_totale
FROM population_commune p
JOIN commune c ON c.code_commune = p.code_commune
JOIN departement d ON d.code_departement = c.code_departement
JOIN departement_region dr ON dr.code_departement = d.code_departement
JOIN region r ON r.code_region = dr.code_region
GROUP BY r.code_region, p.annee;

CREATE OR REPLACE VIEW naissance_region_view AS
SELECT r.code_region, n.annee_debut,n.annee_fin, SUM(n.nombre) AS naissance_totale
FROM naissance_commune n
JOIN commune c ON c.code_commune = n.code_commune
JOIN departement d ON d.code_departement = c.code_departement
JOIN departement_region dr ON dr.code_departement = d.code_departement
JOIN region r ON r.code_region = dr.code_region
GROUP BY r.code_region, n.annee_debut,n.annee_fin;

CREATE OR REPLACE VIEW deces_region_view AS
SELECT r.code_region, dc.annee_debut,dc.annee_fin, SUM(dc.nombre) AS deces_totale
FROM deces_commune dc
JOIN commune c ON c.code_commune = dc.code_commune
JOIN departement d ON d.code_departement = c.code_departement
JOIN departement_region dr ON dr.code_departement = d.code_departement
JOIN region r ON r.code_region = dr.code_region
GROUP BY r.code_region, dc.annee_debut,dc.annee_fin;

CREATE TABLE IF NOT EXISTS population_region (
    CODE_REGION INT REFERENCES region(CODE_REGION),
    ANNEE INT REFERENCES type_annee_population(ANNEE_POPULATION),
    population_totale BIGINT,
    PRIMARY KEY(CODE_REGION,ANNEE)
);

CREATE TABLE IF NOT EXISTS population_departement (
    CODE_DEPARTEMENT VARCHAR(3) REFERENCES departement(CODE_DEPARTEMENT),
    ANNEE INT REFERENCES type_annee_population(ANNEE_POPULATION),
    population_totale BIGINT,
    PRIMARY KEY(CODE_DEPARTEMENT,ANNEE)
);

INSERT INTO population_departement (code_departement, annee, population_totale)
SELECT d.code_departement, pc.annee, SUM(pc.nombre)
FROM departement d
JOIN commune c ON d.code_departement = c.code_departement
JOIN population_commune pc ON c.code_commune = pc.code_commune
GROUP BY d.code_departement, pc.annee;

INSERT INTO population_region (code_region, annee, population_totale)
SELECT r.code_region, pc.annee, SUM(pc.nombre)
FROM region r
JOIN departement_region dr ON r.code_region = dr.code_region
JOIN commune c ON dr.code_departement = c.code_departement
JOIN population_commune pc ON c.code_commune = pc.code_commune
GROUP BY r.code_region, pc.annee;

--Procedure stockée
CREATE OR REPLACE PROCEDURE calculer_population_departement_et_region()
LANGUAGE plpgsql
AS $$
DECLARE
  code_dep VARCHAR(3);
  annee_dep INT;
  pop_dep INT;
  code_reg INT;
  annee_reg INT;
  pop_reg INT;
  dep_cursor CURSOR FOR SELECT code_departement, annee FROM population_departement;
  reg_cursor CURSOR FOR SELECT code_region, annee FROM population_region;
BEGIN
  OPEN dep_cursor;
  LOOP
    FETCH dep_cursor INTO code_dep, annee_dep;
    EXIT WHEN NOT FOUND;

    SELECT SUM(nombre) INTO pop_dep
    FROM population_commune pc
    JOIN commune c ON pc.code_commune = c.code_commune
    WHERE c.code_departement = code_dep AND pc.annee = annee_dep;

    UPDATE population_departement
    SET population_totale = pop_dep
    WHERE code_departement = code_dep AND annee = annee_dep;
  END LOOP;
  CLOSE dep_cursor;

  OPEN reg_cursor;
  LOOP
    FETCH reg_cursor INTO code_reg, annee_reg;
    EXIT WHEN NOT FOUND;

    SELECT SUM(d.population_totale) INTO pop_reg
    FROM population_departement d
    JOIN departement_region dr ON d.code_departement = dr.code_departement
    WHERE dr.code_region = code_reg AND d.annee = annee_reg;

    UPDATE population_region
    SET population_totale = pop_reg
    WHERE code_region = code_reg AND annee = annee_reg;
  END LOOP;
  CLOSE reg_cursor;
END;
$$;


/*Créez une fonction pour lever une exception lorsque des modifications sont tentées sur les tables region et departement.*/
CREATE OR REPLACE FUNCTION bloquer_modifications() RETURNS TRIGGER AS $$
BEGIN
  RAISE EXCEPTION 'Les modifications sur la table % sont interdites.', TG_TABLE_NAME;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

/*Créez des triggers pour bloquer les commandes INSERT, UPDATE et DELETE sur les tables region et departement.*/
CREATE OR REPLACE TRIGGER bloquer_insert_region
BEFORE INSERT ON region
FOR EACH ROW
EXECUTE FUNCTION bloquer_modifications();

CREATE OR REPLACE TRIGGER bloquer_update_region
BEFORE UPDATE ON region
FOR EACH ROW
EXECUTE FUNCTION bloquer_modifications();

CREATE OR REPLACE TRIGGER bloquer_delete_region
BEFORE DELETE ON region
FOR EACH ROW
EXECUTE FUNCTION bloquer_modifications();

CREATE OR REPLACE TRIGGER bloquer_insert_departement
BEFORE INSERT ON departement
FOR EACH ROW
EXECUTE FUNCTION bloquer_modifications();

CREATE OR REPLACE TRIGGER bloquer_update_departement
BEFORE UPDATE ON departement
FOR EACH ROW
EXECUTE FUNCTION bloquer_modifications();

CREATE OR REPLACE TRIGGER bloquer_delete_departement
BEFORE DELETE ON departement
FOR EACH ROW
EXECUTE FUNCTION bloquer_modifications();

/*Créez une fonction pour appeler la procédure stockée calculer_population_departement_et_region() après la mise à jour de la population d'une ville.*/
CREATE OR REPLACE FUNCTION mettre_a_jour_population_apres_update()
RETURNS TRIGGER AS $$
BEGIN
    CALL calculer_population_departement_et_region();
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

/*Créez un trigger pour appeler la fonction mettre_a_jour_population_apres_update() après la mise à jour de la population d'une ville.*/
CREATE OR REPLACE TRIGGER mettre_a_jour_population_apres_update_trigger
AFTER UPDATE OF nombre ON population_commune
FOR EACH ROW
EXECUTE FUNCTION mettre_a_jour_population_apres_update();


-----------------------------------------------------------

-- Trigger après l'insertion dans population_commune
CREATE OR REPLACE FUNCTION update_population_departement_after_insert() RETURNS TRIGGER AS $$
BEGIN
    INSERT INTO population_departement (code_departement, annee, population_totale)
    SELECT c.code_departement, NEW.annee, SUM(NEW.nombre)
    FROM population_commune p
    JOIN commune c ON c.code_commune = p.code_commune
    WHERE c.code_departement = (SELECT code_departement FROM commune WHERE code_commune = NEW.code_commune)
    GROUP BY c.code_departement, NEW.annee
    ON CONFLICT (code_departement, annee) DO UPDATE SET population_totale = (
        SELECT SUM(population_commune.nombre)
        FROM population_commune
        JOIN commune ON commune.code_commune = population_commune.code_commune
        WHERE commune.code_departement = population_departement.code_departement
        AND population_commune.annee = population_departement.annee
    );

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER population_departement_after_insert
AFTER INSERT ON population_commune
FOR EACH ROW
EXECUTE FUNCTION update_population_departement_after_insert();

-- Trigger après l'insertion dans population_departement
CREATE OR REPLACE FUNCTION update_population_region_after_insert() RETURNS TRIGGER AS $$
BEGIN
    INSERT INTO population_region (code_region, annee, population_totale)
    SELECT dr.code_region, NEW.annee, SUM(NEW.population_totale)
    FROM population_departement p
    JOIN departement_region dr ON dr.code_departement = p.code_departement
    WHERE dr.code_region = (SELECT code_region FROM departement_region WHERE code_departement = NEW.code_departement)
    GROUP BY dr.code_region, NEW.annee
    ON CONFLICT (code_region, annee) DO UPDATE SET population_totale = (
        SELECT SUM(population_departement.population_totale)
        FROM population_departement
        JOIN departement_region ON departement_region.code_departement = population_departement.code_departement
        WHERE departement_region.code_region = population_region.code_region
        AND population_departement.annee = population_region.annee
    );

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;



CREATE TRIGGER update_population_region_trigger
AFTER INSERT ON population_departement
FOR EACH ROW
EXECUTE FUNCTION update_population_region_after_insert();

import psycopg2
import threading
import time
import json
with open('bdwebflask/config.json', 'r') as f:
    config = json.load(f)
def connect():
    return psycopg2.connect(
        dbname=config['dbname'],
        user=config['user'],
        password=config['password'],
        host=config['host']
    )
# Fonction pour la première transaction
def transaction1():
    conn =connect()
    cursor = conn.cursor()
    cursor.execute("SET TRANSACTION ISOLATION LEVEL SERIALIZABLE")
    cursor.execute("BEGIN")
    cursor.execute("SELECT nombre FROM population_commune WHERE population_commune.code_commune = '33365' AND annee='2020'")
    result = cursor.fetchone()
    print(f"Transaction 1 : valeur initiale = {result[0]}")
    time.sleep(5)  # Attendre pendant 5 secondes
    cursor.execute("SELECT nombre FROM population_commune WHERE population_commune.code_commune = '33365' AND annee='2020'")
    result = cursor.fetchone()
    print(f"Transaction 1 : valeur après l'attente = {result[0]}")
    cursor.execute("COMMIT")
    conn.close()

# Fonction pour la deuxième transaction
def transaction2():
    conn =connect()
    cursor = conn.cursor()
    cursor.execute("SET TRANSACTION ISOLATION LEVEL SERIALIZABLE")
    cursor.execute("BEGIN")
    time.sleep(1)  # Attendre pendant 1 seconde pour laisser la transaction 1 lire la valeur
    cursor.execute("UPDATE population_commune SET nombre = 200 WHERE code_commune = '33365' AND annee = '2020'")
    time.sleep(1)  # Attendre pendant 5 secondes
    cursor.execute("COMMIT")
    time.sleep(5)
    cursor.execute("UPDATE population_commune SET nombre = 199 WHERE code_commune = '33365' AND annee = '2020'")
    print("valeur remise a défaut ")
    conn.close()
def transaction3():
    conn =connect()
    cursor = conn.cursor()
    cursor.execute("SET TRANSACTION ISOLATION LEVEL READ COMMITTED")
    cursor.execute("BEGIN")
    cursor.execute("SELECT nombre FROM population_commune WHERE population_commune.code_commune = '33365' AND annee='2020'")
    result = cursor.fetchone()
    print(f"Transaction 3 : valeur initiale = {result[0]}")
    time.sleep(5)  # Attendre pendant 5 secondes
    cursor.execute("SELECT nombre FROM population_commune WHERE population_commune.code_commune = '33365' AND annee='2020'")
    result = cursor.fetchone()
    print(f"Transaction 3 : valeur après l'attente = {result[0]}")
    cursor.execute("COMMIT")
    conn.close()

# Fonction pour la deuxième transaction
def transaction4():
    conn =connect()
    cursor = conn.cursor()
    cursor.execute("SET TRANSACTION ISOLATION LEVEL  READ COMMITTED")
    cursor.execute("BEGIN")
    time.sleep(1)  # Attendre pendant 1 seconde pour laisser la transaction 1 lire la valeur
    cursor.execute("UPDATE population_commune SET nombre = 200 WHERE code_commune = '33365' AND annee = '2020'")
    time.sleep(1)  # Attendre pendant 5 secondes
    cursor.execute("COMMIT")
    time.sleep(5)
    cursor.execute("UPDATE population_commune SET nombre = 199 WHERE code_commune = '33365' AND annee = '2020'")
    print("valeur remise a défaut ")
    conn.close()
def transaction5():
    conn =connect()
    cursor = conn.cursor()
    cursor.execute("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED")
    cursor.execute("BEGIN")
    cursor.execute("SELECT nombre FROM population_commune WHERE population_commune.code_commune = '33365' AND annee='2020'")
    result = cursor.fetchone()
    print(f"Transaction 5 : valeur initiale = {result[0]}")
    time.sleep(5)  # Attendre pendant 5 secondes
    cursor.execute("SELECT nombre FROM population_commune WHERE population_commune.code_commune = '33365' AND annee='2020'")
    result = cursor.fetchone()
    print(f"Transaction 5 : valeur après l'attente = {result[0]}")
    cursor.execute("COMMIT")
    conn.close()

# Fonction pour la deuxième transaction
def transaction6():
    conn =connect()
    cursor = conn.cursor()
    cursor.execute("SET TRANSACTION ISOLATION LEVEL  READ UNCOMMITTED")
    cursor.execute("BEGIN")
    time.sleep(1)  # Attendre pendant 1 seconde pour laisser la transaction 1 lire la valeur
    cursor.execute("UPDATE population_commune SET nombre = 200 WHERE code_commune = '33365' AND annee = '2020'")
    time.sleep(1)  # Attendre pendant 5 secondes
    cursor.execute("COMMIT")
    time.sleep(5)
    cursor.execute("UPDATE population_commune SET nombre = 199 WHERE code_commune = '33365' AND annee = '2020'")
    print("valeur remise a défaut ")
    conn.close()
print("SERIALIZABLE")
# Créer les threads pour les deux transactions
thread1 = threading.Thread(target=transaction1)
thread2 = threading.Thread(target=transaction2)

# Démarrer les threads
thread1.start()
thread2.start()

# Attendre que les threads se terminent
thread1.join()
thread2.join()
print( "READ COMMITTED")
# Créer les threads pour les deux transactions
thread3 = threading.Thread(target=transaction3)
thread4 = threading.Thread(target=transaction4)

# Démarrer les threads
thread3.start()
thread4.start()

# Attendre que les threads se terminent
thread3.join()
thread4.join()
print( "READ UNCOMMITTED")
# Créer les threads pour les deux transactions
thread3 = threading.Thread(target=transaction5)
thread4 = threading.Thread(target=transaction6)

# Démarrer les threads
thread3.start()
thread4.start()

# Attendre que les threads se terminent
thread3.join()
thread4.join()
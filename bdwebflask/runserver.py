import psycopg2
from flask import Flask, redirect, render_template, send_file,request, url_for
import io
import json
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import numpy as np
import pandas as pd
import time

with open('config.json', 'r') as f:
    config = json.load(f)
def connect():
    return psycopg2.connect(
        dbname=config['dbname'],
        user=config['user'],
        password=config['password'],
        host=config['host']
    )
class Commune:
    def __init__(self, code_commune, libelle):
        self.code_commune = code_commune
        self.libelle = libelle
app = Flask(__name__)

# Connexion à la base de données PostgreSQL
try:
    conn=connect()
    print("Connexion réussie à la base de données PostgreSQL.")
    cursor = conn.cursor()
    cursor.execute("SET TRANSACTION ISOLATION LEVEL SERIALIZABLE")


except Exception as e:
    print(f"Erreur lors de la connexion à la base de données PostgreSQL: {e}")

@app.route('/')
def index():
    return render_template('index.html')

#---------------------------------------Performance de la base ----------------------------------------------------------
def execute_explain_query(cursor, query):
    cursor.execute(query)
    results = cursor.fetchall()
    columns = [desc[0] for desc in cursor.description]
    df = pd.DataFrame(results, columns=columns) 
    return df
@app.route('/report')
def report():
    return render_template('report.html')
@app.route('/performance',methods=['POST'])
def performance():
    cursor = conn.cursor()
    try:
        # Liste pour stocker les DataFrames
        dfs = []

        # Liste pour stocker les commandes SQL
        commands = []
            # Requête avec jointure imbriquée
        query = """EXPLAIN (ANALYZE, VERBOSE, BUFFERS) SELECT *
                    FROM mariage
                    WHERE CODE_DEPARTEMENT IN (
                        SELECT departement.CODE_DEPARTEMENT
                        FROM departement
                        WHERE departement.CHEFLIEU IN (
                            SELECT commune.CODE_COMMUNE
                            FROM commune
                        )
                    )
                    AND AGE_DEBUT >= 20 AND AGE_FIN <= 30"""
        df = execute_explain_query(cursor, query)
        dfs.append(df)
        commands.append("6) Requête avec jointure imbriquée "+query)

        # Requête avec jointure de hachage
        query = """EXPLAIN (ANALYZE, VERBOSE, BUFFERS) SELECT *
FROM mariage
JOIN (
    SELECT departement.CODE_DEPARTEMENT, departement.CHEFLIEU
    FROM departement
    JOIN commune ON departement.CHEFLIEU = commune.CODE_COMMUNE
) AS departement_cheflieu ON mariage.CODE_DEPARTEMENT = departement_cheflieu.CODE_DEPARTEMENT
WHERE mariage.AGE_DEBUT >= 20 AND mariage.AGE_FIN <= 30"""
        df = execute_explain_query(cursor, query)
        dfs.append(df)
        commands.append("Requête avec jointure de hachage "+query)

        # Requête avec jointure méridienne
        query = """EXPLAIN (ANALYZE, VERBOSE, BUFFERS) SELECT *
FROM mariage
WHERE CODE_DEPARTEMENT IN (
    SELECT departement.CODE_DEPARTEMENT
    FROM departement
    WHERE departement.CHEFLIEU IN (
        SELECT commune.CODE_COMMUNE
        FROM commune
    )
)
AND AGE_DEBUT >= 20 AND AGE_FIN <= 30"""
        df = execute_explain_query(cursor, query)
        dfs.append(df)
        commands.append("Requête avec jointure méridienne "+query)

        query = "EXPLAIN (ANALYZE, VERBOSE, BUFFERS) SELECT * FROM commune"
        df = execute_explain_query(cursor, query)
        dfs.append(df)
        commands.append(query)

        query = "EXPLAIN (ANALYZE, VERBOSE, BUFFERS) SELECT * FROM mariage_etat_matrimoniale_anterieur WHERE CODE_DEPARTEMENT = '33'"
        df = execute_explain_query(cursor, query)
        dfs.append(df)
        commands.append(query)

        query = "EXPLAIN (ANALYZE, VERBOSE, BUFFERS) SELECT * FROM mariage_repartition_mensuelle WHERE CODE_DEPARTEMENT = '92'"
        df = execute_explain_query(cursor, query)
        dfs.append(df)
        commands.append(query)
        #7 procedure index
        query = "EXPLAIN (ANALYZE, VERBOSE, BUFFERS) SELECT * FROM commune NATURAL JOIN population_commune WHERE population_commune.nombre <5000 and population_commune.annee='2020' "
        df = execute_explain_query(cursor, query)
        dfs.append(df)
        commands.append("7)SANS INDEX "+query)
        # Fermeture du curseur
        
        cursor.execute("CREATE INDEX idx_population_commune_nombre ON population_commune (nombre)")
        conn.commit()
        query = "EXPLAIN (ANALYZE, VERBOSE, BUFFERS) SELECT * FROM commune NATURAL JOIN population_commune WHERE population_commune.nombre <5000 and population_commune.annee='2020' "
        df = execute_explain_query(cursor, query)
        dfs.append(df)
        commands.append("AVEC INDEX  "+query)
        cursor.execute("DROP INDEX idx_population_commune_nombre")
        conn.commit()
        cursor.close()

        # Création d'une liste de tuples contenant chaque commande et son DataFrame correspondant
        tables_with_commands = list(zip(commands, [df.to_html(index=False) for df in dfs]))

        return render_template('performance.html', tables_with_commands=tables_with_commands)
    except Exception as e:
        conn.rollback()
        cursor.close()
        raise e
    except Exception as e:
        conn.rollback()
        cursor.close()
#---------------------------------------Affichage Commune Departement Region --------------------------------------------
@app.route('/region', methods=['POST'])
def region():
    cursor = conn.cursor()
    try:
        cursor.execute("SELECT nom_majuscule,code_region FROM Region ORDER BY nom_majuscule")
        regions = cursor.fetchall()
        cursor.close()
        return render_template('regions.html', regions=regions)
    except Exception as e:
        conn.rollback()
        cursor.close()
        raise e
@app.route('/departements/<int:code_region>/<string:nom_region>', methods=['GET'])
def departements(code_region,nom_region):
    cursor = conn.cursor()
    try:
        cursor.execute("""
            SELECT DISTINCT Departement.nom_majuscule,Departement.code_departement
            FROM Departement JOIN departement_region ON Departement.code_departement =departement_region.code_departement
            JOIN Region ON departement_region.code_region = %s
            ORDER BY nom_majuscule
        """, (code_region,))
        departements = cursor.fetchall()
        print(departements)
        cursor.execute("""
        SELECT code_commune, libelle FROM Commune
        WHERE code_commune = (
            SELECT cheflieu
            FROM Region
            WHERE code_region = %s
            )
    """, (code_region,))
        result1 = cursor.fetchall()
        cursor.execute("""
        SELECT population FROM Region
        WHERE code_region = %s
    """, (code_region,))
        result2 = cursor.fetchall()
        cursor.close()
        if result1:
            cheflieux = [Commune(code_commune, libelle) for code_commune, libelle in result1]
        else:
            cheflieux = []
        return render_template('departements.html', departements=departements, nom_region=nom_region,code_region=code_region,cheflieux=cheflieux,population=result2)
    except Exception as e:
        conn.rollback()
        cursor.close()
        raise e
@app.route('/communes/<string:code_departement>/<string:nom_departement>', methods=['GET'])
def communes(code_departement,nom_departement):
    
    cursor = conn.cursor()
    try:
        cursor.execute("""
        SELECT Commune.code_commune, Commune.libelle
        FROM Commune
        WHERE Commune.code_departement = %s
        ORDER BY libelle
    """, (code_departement,))
        result = cursor.fetchall()
        communes = [Commune(code_commune, libelle) for code_commune, libelle in result]
        cursor.execute("""
        SELECT code_commune, libelle FROM Commune
        WHERE code_departement = %s AND code_commune = (
            SELECT cheflieu
            FROM Departement
            WHERE code_departement = %s
            )
    """, (code_departement,code_departement,))
        result1 = cursor.fetchall()
        cursor.execute("""
        SELECT population FROM Departement
        WHERE code_departement = %s
    """, (code_departement,))
        result2 = cursor.fetchall()
        cursor.close()
        
        if result1:
            cheflieux = [Commune(code_commune, libelle) for code_commune, libelle in result1]
        else:
            cheflieux = []

        print("chef",result1)
        return render_template('communes.html', communes=communes,code_departement=code_departement, nom_departement=nom_departement,cheflieux=cheflieux,population=result2)
    except Exception as e:
        conn.rollback()
        cursor.close()
        raise e
@app.route('/commune/<string:code_commune>/<string:nom_commune>')
def commune(code_commune,nom_commune):

    cursor = conn.cursor()
    try:
        code_commune = str(code_commune).zfill(5)

        return render_template('commune.html', code_commune=code_commune,nom_commune=nom_commune)
    except Exception as e:
        conn.rollback()
        cursor.close()
        raise e
#-----------------------STATS---------------------------------------------------------------------------
@app.route('/mariage/<string:type>/<string:code>/<string:nom>')
def mariage(type,code,nom):
    cursor = conn.cursor()
    try:   
        if type == 'departement':
            code = str(code).zfill(2)
            cursor.execute("SELECT annee, population_totale FROM population_departement_view WHERE population_departement_view.code_departement = %s ORDER BY population_departement_view.annee ASC", (str(code),))
        elif type == 'region':
            code = str(code).zfill(2)
            cursor.execute("SELECT annee, population_totale FROM population_region_view WHERE population_region_view.code_region = %s ORDER BY population_region_view.annee ASC", (str(code),))
        else:
            return
        data = cursor.fetchall()
        print(data)
        cursor.close()
        return render_template('mariage.html', code=code, data=data,nom=nom,type=type)
    except Exception as e:
        print(e)
        conn.rollback()
        cursor.close()
        raise e
@app.route('/population/<string:type>/<string:code>/<string:nom>')
def population(type,code,nom):
    cursor = conn.cursor()
    try:
        if type == 'commune':
            code = str(code).zfill(5)
            cursor.execute("SELECT annee, nombre FROM population_commune WHERE population_commune.code_commune = %s", (str(code),))
        elif type == 'departement':
            code = str(code).zfill(2)
            cursor.execute("SELECT annee, population_totale FROM population_departement_view WHERE population_departement_view.code_departement = %s ORDER BY population_departement_view.annee ASC", (str(code),))
        elif type == 'region':
            code = str(code).zfill(2)
            cursor.execute("SELECT annee, population_totale FROM population_region_view WHERE population_region_view.code_region = %s ORDER BY population_region_view.annee ASC", (str(code),))

        data = cursor.fetchall()
        print(data)
        cursor.close()
        return render_template('population.html', code=code, data=data,nom=nom,type=type)
    except Exception as e:
        print(e)
        conn.rollback()
        cursor.close()
        raise e
@app.route('/deces/<string:type>/<string:code>/<string:nom>')
def deces(type,code,nom):
    cursor = conn.cursor()
    try:
        if type == 'commune':
            code = str(code).zfill(5)
            cursor.execute("SELECT annee_debut,annee_fin, nombre FROM deces_commune WHERE deces_commune.code_commune = %s", (str(code),))
        elif type == 'departement':
            code = str(code).zfill(2)
            cursor.execute("SELECT annee_debut,annee_fin, deces_totale FROM deces_departement_view WHERE deces_departement_view.code_departement = %s ORDER BY deces_departement_view.annee_debut ASC", (str(code),))
        elif type == 'region':
            code = str(code).zfill(2)
            cursor.execute("SELECT annee_debut,annee_fin, deces_totale FROM deces_region_view WHERE deces_region_view.code_region = %s ORDER BY deces_region_view.annee_debut ASC", (str(code),))
        
        data = cursor.fetchall()
        print(data)
        cursor.close()
        return render_template('deces.html', code=code, data=data,nom=nom,type=type)
    
    except Exception as e:
        print(e)
        conn.rollback()
        cursor.close()
        raise e
    
@app.route('/naissance/<string:type>/<string:code>/<string:nom>')
def naissance(type,code,nom):
    cursor = conn.cursor()
    try:
        if type == 'commune':
            code = str(code).zfill(5)
            cursor.execute("SELECT annee_debut,annee_fin, nombre FROM naissance_commune WHERE naissance_commune.code_commune = %s", (str(code),))
        elif type == 'departement':
            code = str(code).zfill(2)
            cursor.execute("SELECT annee_debut,annee_fin, naissance_totale FROM naissance_departement_view WHERE naissance_departement_view.code_departement = %s ORDER BY naissance_departement_view.annee_debut ASC", (str(code),))
        elif type == 'region':
            code = str(code).zfill(2)
            cursor.execute("SELECT annee_debut,annee_fin, naissance_totale FROM naissance_region_view WHERE naissance_region_view.code_region = %s ORDER BY naissance_region_view.annee_debut ASC", (str(code),))
        data = cursor.fetchall()
        print(data)
        cursor.close()
        return render_template('naissance.html', code=code, data=data,nom=nom,type=type)
    except Exception as e:
        print(e)
        conn.rollback()
        cursor.close()
        raise e

@app.route('/graphique/<string:type>/<string:code>')
def graphique_population(type,code):
    # Récupération des données de la table SQL
    cursor = conn.cursor()
    try:
        if type == 'commune':
            code = str(code).zfill(5)
            cursor.execute("SELECT annee, nombre FROM population_commune WHERE code_commune = %s", (str(code),))
        elif type == 'departement':
            code = str(code).zfill(2)
            cursor.execute("SELECT annee, population_totale FROM population_departement_view WHERE population_departement_view.code_departement = %s ORDER BY population_departement_view.annee ASC", (str(code),))
        elif type == 'region':
            code = str(code).zfill(2)
            cursor.execute("SELECT annee, population_totale FROM population_region_view WHERE population_region_view.code_region = %s ORDER BY population_region_view.annee ASC", (str(code),))

        data = cursor.fetchall()
        cursor.close()
        # Création du graphique
        fig, ax = plt.subplots()
        ax.plot([x[0] for x in data], [x[1] for x in data])
        ax.set_xlabel("Année")
        ax.set_ylabel("Population")

        # Enregistrement du graphique dans un fichier en mémoire
        buf = io.BytesIO()
        fig.savefig(buf, format='png')
        buf.seek(0)

        # Fermeture de la figure Matplotlib
        plt.close(fig)

        # Renvoi du fichier en mémoire contenant le graphique
        return send_file(buf, mimetype='image/png')
    except Exception as e:
        print(e)
        conn.rollback()
        cursor.close()
        raise e
    
@app.route('/graphique_naissance/<string:type>/<string:code>')
def graphique_naissance(type,code):
    # Récupération des données de la table SQL
    cursor = conn.cursor()
    try:
        if type == 'commune':
            code = str(code).zfill(5)
            cursor.execute("SELECT annee_debut,annee_fin, nombre FROM naissance_commune WHERE naissance_commune.code_commune = %s", (str(code),))
        elif type == 'departement':
            code = str(code).zfill(2)
            cursor.execute("SELECT annee_debut,annee_fin, naissance_totale FROM naissance_departement_view WHERE naissance_departement_view.code_departement = %s ORDER BY naissance_departement_view.annee_debut ASC", (str(code),))
        elif type == 'region':
            code = str(code).zfill(2)
            cursor.execute("SELECT annee_debut,annee_fin, naissance_totale FROM naissance_region_view WHERE naissance_region_view.code_region = %s ORDER BY naissance_region_view.annee_debut ASC", (str(code),))

        data = cursor.fetchall()
        cursor.close()
        n = len(data)
        w = 8 + 2 * (n // 50)
        h = 6 + 1 * (n // 50)
        fig = plt.figure(figsize=(w, h))
        ax = fig.add_subplot(111)
        x_labels = [f'{x[0]}-{x[1]}' for x in data]
        ax.plot(x_labels, [x[2] for x in data])
        plt.xticks(np.arange(len(x_labels)), x_labels, rotation=45)
        ax.set_xlabel("Année")
        ax.set_ylabel("Naissance")

     
        # Enregistrement du graphique dans un fichier en mémoire
        buf = io.BytesIO()
        fig.savefig(buf, format='png')
        buf.seek(0)

        # Fermeture de la figure Matplotlib
        plt.close(fig)

        # Renvoi du fichier en mémoire contenant le graphique
        return send_file(buf, mimetype='image/png')
    except Exception as e:
        print(e)
        conn.rollback()
        cursor.close()
        raise e
@app.route('/graphique_deces/<string:type>/<string:code>')
def graphique_deces(type,code):
    # Récupération des données de la table SQL
    cursor = conn.cursor()
    try:
        if type == 'commune':
            code = str(code).zfill(5)
            cursor.execute("SELECT annee_debut,annee_fin, nombre FROM deces_commune WHERE deces_commune.code_commune = %s", (str(code),))
        elif type == 'departement':
            code = str(code).zfill(2)
            cursor.execute("SELECT annee_debut,annee_fin, deces_totale FROM deces_departement_view WHERE deces_departement_view.code_departement = %s ORDER BY deces_departement_view.annee_debut ASC", (str(code),))
        elif type == 'region':
            code = str(code).zfill(2)
            cursor.execute("SELECT annee_debut,annee_fin, deces_totale FROM deces_region_view WHERE deces_region_view.code_region = %s ORDER BY deces_region_view.annee_debut ASC", (str(code),))
        data = cursor.fetchall()
        cursor.close()
        n = len(data)
        w = 8 + 2 * (n // 50)
        h = 6 + 1 * (n // 50)
        fig = plt.figure(figsize=(w, h))
        ax = fig.add_subplot(111)
        x_labels = [f'{x[0]}-{x[1]}' for x in data]
        ax.plot(x_labels, [x[2] for x in data])
        plt.xticks(np.arange(len(x_labels)), x_labels, rotation=45)
        ax.set_xlabel("Année")
        ax.set_ylabel("Décès")

     
        # Enregistrement du graphique dans un fichier en mémoire
        buf = io.BytesIO()
        fig.savefig(buf, format='png')
        buf.seek(0)

        # Fermeture de la figure Matplotlib
        plt.close(fig)

        # Renvoi du fichier en mémoire contenant le graphique
        return send_file(buf, mimetype='image/png')
    except Exception as e:
        print(e)
        conn.rollback()
        cursor.close()
        raise e
    
#---------------------------------------MODIF STATS --------------------------------------------
@app.route('/modification/<string:type>/<string:code>/<string:nom>')
def modification(type,code,nom):
    cursor = conn.cursor()
    try:
        if type == 'commune':
            code = str(code).zfill(5)
            cursor.execute("SELECT annee, nombre FROM population_commune WHERE population_commune.code_commune = %s", (str(code),))
        elif type == 'departement':
            code = str(code).zfill(2)
            cursor.execute("SELECT annee, population_totale FROM population_departement_view WHERE population_departement_view.code_departement = %s ORDER BY population_departement_view.annee ASC", (str(code),))
        elif type == 'region':
            code = str(code).zfill(2)
            cursor.execute("SELECT annee, population_totale FROM population_region_view WHERE population_region_view.code_region = %s ORDER BY population_region_view.annee ASC", (str(code),))

        data = cursor.fetchall()
        print(data)
        cursor.close()
        return render_template('modification_population.html', code=code, data=data,nom=nom,type=type)
    except Exception as e:
        print(e)
        conn.rollback()
        cursor.close()
        raise e
@app.route('/modify_population/<string:type>/<string:code>/<string:nom>', methods=['POST'])
def modify_population(type, code,nom):
    annee = request.form['annee']
    population = request.form['population']

    # Mettre à jour les données dans la base de données
    cursor = conn.cursor()
    try:
        if type == 'commune':
            code = str(code).zfill(5)
            cursor.execute("UPDATE population_commune SET nombre = %s WHERE code_commune = %s AND annee = %s", (population, code, annee))
        elif type == 'departement':
            code = str(code).zfill(2)
            cursor.execute("UPDATE population_departement_view SET population_totale = %s WHERE code_departement = %s AND annee = %s", (population, code, annee))
        elif type == 'region':
            code = str(code).zfill(2)
            cursor.execute("UPDATE population_region_view SET population_totale = %s WHERE code_region = %s AND annee = %s", (population, code, annee))

        conn.commit()
    except Exception as e:
        print(e)
        conn.rollback()

    return redirect(url_for('population', type=type, code=code,nom=nom))
if __name__ == '__main__':
    app.run(port=8085,debug=True)

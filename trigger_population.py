from io import StringIO
import psycopg2
import pandas as pd
import json
with open('bdwebflask/config.json', 'r') as f:
    config = json.load(f)
def connect():
    return psycopg2.connect(
        dbname=config['dbname'],
        user=config['user'],
        password=config['password'],
        host=config['host']
    )
# Connexion à la base de données PostgreSQL
try:
    conn = connect()
    print("Connexion réussie à la base de données PostgreSQL.")
except Exception as e:
    print(f"Erreur lors de la connexion à la base de données PostgreSQL: {e}")
    
cursor = conn.cursor()

# Import des fichiers CSV dans la base de données
if conn is not None:
    try:
        # Lire le fichier CSV et créer un DataFrame
        df_com = pd.read_csv('data/v_commune_2023.csv', delimiter=',', header=None, names=['TYPECOM', 'COM', 'REG','DEP','CTCD', 'ARR', 'TNCC', 'NCC', 'NCCENR', 'LIBELLE', 'CAN', 'COMPARENT'],skiprows=1)
        df_serie_hist = pd.read_csv('data/base-cc-serie-historique-2020.csv', delimiter=';', header=None, names=['CODGEO', 'P20_POP', 'P14_POP', 'P09_POP', 'D99_POP', 'D90_POP',
       'D82_POP', 'D75_POP', 'D68_POP', 'SUPERF', 'NAIS1420', 'NAIS0914',
       'NAIS9909', 'NAIS9099', 'NAIS8290', 'NAIS7582', 'NAIS6875', 'DECE1420',
       'DECE0914', 'DECE9909', 'DECE9099', 'DECE8290', 'DECE7582', 'DECE6875',
       'P20_LOG', 'P14_LOG', 'P09_LOG', 'D99_LOG', 'D90_LOG', 'D82_LOG',
       'D75_LOG', 'D68_LOG', 'P20_RP', 'P14_RP', 'P09_RP', 'D99_RP', 'D90_RP',
       'D82_RP', 'D75_RP', 'D68_RP', 'P20_RSECOCC', 'P14_RSECOCC',
       'P09_RSECOCC', 'D99_RSECOCC', 'D90_RSECOCC', 'D82_RSECOCC',
       'D75_RSECOCC', 'D68_RSECOCC', 'P20_LOGVAC', 'P14_LOGVAC', 'P09_LOGVAC',
       'D99_LOGVAC', 'D90_LOGVAC', 'D82_LOGVAC', 'D75_LOGVAC', 'D68_LOGVAC',
       'P20_PMEN', 'P14_PMEN', 'P09_PMEN', 'D99_PMEN', 'D90_NPER_RP',
       'D82_NPER_RP', 'D75_NPER_RP', 'D68_NPER_RP'],skiprows=1)

        listCOM = []

        for index, row in df_com.iterrows():
            if row['TYPECOM']!="COM":
                continue
            #Insertion dans la table Communes
            listCOM.append(row['COM'])

        for index, row in df_serie_hist.iterrows():
            codgeo = str(row['CODGEO']).zfill(5)
            if codgeo not in listCOM:
                continue

            #Insertion dans la table Population_Commune pour l'année 1982
            cursor.execute("INSERT INTO Population_Commune (CODE_COMMUNE, ANNEE, NOMBRE) VALUES (%s, %s, %s)",
                        (codgeo,1982,row['D82_POP']))
            
            #Insertion dans la table Population_Commune pour l'année 1975
            cursor.execute("INSERT INTO Population_Commune (CODE_COMMUNE, ANNEE, NOMBRE) VALUES (%s, %s, %s)",
                        (codgeo,1975,row['D75_POP']))
            
            #Insertion dans la table Population_Commune pour l'année 1968
            cursor.execute("INSERT INTO Population_Commune (CODE_COMMUNE, ANNEE, NOMBRE) VALUES (%s, %s, %s)",
                        (codgeo,1968,row['D68_POP']))
            
        # Valider la transaction
        conn.commit()
                         
        
        conn.commit()
        print("Données importées avec succès.")
    
    except Exception as e:
        print(f"Erreur lors de l'importation des données: {e}")

    # Fermeture de la connexion à la base de données
    conn.close()
    print("Connexion à la base de données PostgreSQL fermée.")
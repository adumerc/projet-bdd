CREATE TABLE IF NOT EXISTS type_article (
    CODE_ARTICLE INT PRIMARY KEY,
    DESCRIPTION VARCHAR(200),
    CHARNIERE VARCHAR(200)
);

CREATE TABLE IF NOT EXISTS type_annee_population (
    ANNEE_POPULATION INT PRIMARY KEY,
    DESCRIPTION VARCHAR(200)
);

CREATE TABLE IF NOT EXISTS type_intervalle_age (
    AGE_DEBUT INT,
    AGE_FIN INT,
    DESCRIPTION VARCHAR(200),
    PRIMARY KEY(AGE_DEBUT,AGE_FIN)
);

CREATE TABLE IF NOT EXISTS commune (
    CODE_COMMUNE VARCHAR(5) PRIMARY KEY,
    CODE_COLLECTIVITE_TERRITORIALE VARCHAR(4),
    CODE_ARTICLE INT REFERENCES type_article(CODE_ARTICLE),
    NOM_MAJUSCULE VARCHAR(200),
    NOM_RICHE VARCHAR(200),
    LIBELLE VARCHAR(200)
);

CREATE TABLE IF NOT EXISTS region (
    CODE_REGION INT PRIMARY KEY,
    CHEFLIEU VARCHAR(5) REFERENCES commune( CODE_COMMUNE),
    CODE_ARTICLE INT REFERENCES type_article(CODE_ARTICLE),
    NOM_MAJUSCULE VARCHAR(200),
    NOM_RICHE VARCHAR(200),
    LIBELLE VARCHAR(200)
);

CREATE TABLE IF NOT EXISTS departement (
    CODE_DEPARTEMENT VARCHAR(3) PRIMARY KEY,
    CHEFLIEU VARCHAR(5) REFERENCES commune(CODE_COMMUNE),
    CODE_ARTICLE INT REFERENCES type_article(CODE_ARTICLE),
    NOM_MAJUSCULE VARCHAR(200),
    NOM_RICHE VARCHAR(200),
    LIBELLE VARCHAR(200)
);

CREATE TABLE IF NOT EXISTS departement_region (
    CODE_DEPARTEMENT VARCHAR(3)  PRIMARY KEY REFERENCES departement(CODE_DEPARTEMENT),
    CODE_REGION INT REFERENCES region(CODE_REGION)
);

CREATE TABLE IF NOT EXISTS deces_commune (
    CODE_COMMUNE VARCHAR(5) REFERENCES commune(CODE_COMMUNE),
    ANNEE_DEBUT INT REFERENCES type_annee_population(ANNEE_POPULATION),
    ANNEE_FIN INT REFERENCES type_annee_population(ANNEE_POPULATION),
    NOMBRE INT,
    PRIMARY KEY(CODE_COMMUNE,ANNEE_DEBUT,ANNEE_FIN)
);

CREATE TABLE IF NOT EXISTS naissance_commune (
    CODE_COMMUNE VARCHAR(5) REFERENCES commune(CODE_COMMUNE),
    ANNEE_DEBUT INT REFERENCES type_annee_population(ANNEE_POPULATION),
    ANNEE_FIN INT REFERENCES type_annee_population(ANNEE_POPULATION),
    NOMBRE INT,
    PRIMARY KEY(CODE_COMMUNE,ANNEE_DEBUT,ANNEE_FIN)
);

CREATE TABLE IF NOT EXISTS population_commune (
    CODE_COMMUNE VARCHAR(5) REFERENCES commune(CODE_COMMUNE),
    ANNEE INT REFERENCES type_annee_population(ANNEE_POPULATION),
    NOMBRE INT,
    PRIMARY KEY(CODE_COMMUNE,ANNEE)
);

CREATE TABLE IF NOT EXISTS type_mariage (
    MARIAGE VARCHAR(2) PRIMARY KEY,
    DESCRIPTION VARCHAR(200)
);

CREATE TABLE IF NOT EXISTS type_sexe (
    SEXE VARCHAR(2) PRIMARY KEY,
    DESCRIPTION VARCHAR(200)
);

CREATE TABLE IF NOT EXISTS type_etat_matrimoniale (
    ETAT_MATRIMONIALE CHAR PRIMARY KEY,
    DESCRIPTION VARCHAR(200)
);

CREATE TABLE IF NOT EXISTS type_mixite_epoux (
    MIXITE_EPOUX VARCHAR(7) PRIMARY KEY,
    DESCRIPTION VARCHAR(200)
);

CREATE TABLE IF NOT EXISTS mois_mariage (
    MOIS INT PRIMARY KEY,
    DESCRIPTION VARCHAR(200)
);


CREATE TABLE IF NOT EXISTS mariage (
    CODE_DEPARTEMENT VARCHAR(3) REFERENCES departement(CODE_DEPARTEMENT),
    AGE_DEBUT INT,
    AGE_FIN INT,
    TYPE_MARIAGE VARCHAR(2) REFERENCES type_mariage(MARIAGE),
    SEXE CHAR REFERENCES type_sexe(sexe),
    NOMBRE INT,
    FOREIGN KEY (AGE_DEBUT,AGE_FIN) REFERENCES type_intervalle_age(AGE_DEBUT,AGE_FIN),
    PRIMARY KEY(CODE_DEPARTEMENT,AGE_DEBUT,AGE_FIN,SEXE,TYPE_MARIAGE)
);

CREATE TABLE IF NOT EXISTS mariage_etat_matrimoniale_anterieur (
    CODE_DEPARTEMENT VARCHAR(3) REFERENCES departement(CODE_DEPARTEMENT),
    ETAT_MATRIMONIALE CHAR REFERENCES type_etat_matrimoniale(ETAT_MATRIMONIALE),
    SEXE CHAR REFERENCES type_sexe(sexe),
    TYPE_MARIAGE VARCHAR(2) REFERENCES type_mariage(MARIAGE),
    NOMBRE INT,
    PRIMARY KEY(CODE_DEPARTEMENT,ETAT_MATRIMONIALE,SEXE,TYPE_MARIAGE)
);

CREATE TABLE IF NOT EXISTS mariage_nationalite_pays_epoux (
    CODE_DEPARTEMENT VARCHAR(3) REFERENCES departement(CODE_DEPARTEMENT),
    TYPE_MARIAGE VARCHAR(2) REFERENCES type_mariage(MARIAGE),
    MIXITE_EPOUX VARCHAR(7) REFERENCES type_mixite_epoux(MIXITE_EPOUX),
    NOMBRE_NATIONALITE INT,
    NOMBRE_PAYS_NAISSANCE INT,
    PRIMARY KEY(CODE_DEPARTEMENT,TYPE_MARIAGE,MIXITE_EPOUX)
);

CREATE TABLE IF NOT EXISTS mariage_repartition_mensuelle (
    CODE_DEPARTEMENT VARCHAR(3) REFERENCES departement(CODE_DEPARTEMENT),
    TYPE_MARIAGE VARCHAR(2) REFERENCES type_mariage(MARIAGE),
    MOIS INT REFERENCES mois_mariage(MOIS),
    NOMBRE INT,
    PRIMARY KEY(CODE_DEPARTEMENT,TYPE_MARIAGE,MOIS)
);
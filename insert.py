from io import StringIO
import psycopg2
import pandas as pd
import json
with open('bdwebflask/config.json', 'r') as f:
    config = json.load(f)
def connect():
    return psycopg2.connect(
        dbname=config['dbname'],
        user=config['user'],
        password=config['password'],
        host=config['host']
    )
# Connexion à la base de données PostgreSQL
try:
    conn = connect()
    print("Connexion réussie à la base de données PostgreSQL.")
except Exception as e:
    print(f"Erreur lors de la connexion à la base de données PostgreSQL: {e}")
    
cursor = conn.cursor()

# Import des fichiers CSV dans la base de données
if conn is not None:
    try:
        # Lire le fichier CSV et créer un DataFrame
        df_com = pd.read_csv('data/v_commune_2023.csv', delimiter=',', header=None, names=['TYPECOM', 'COM', 'REG','DEP','CTCD', 'ARR', 'TNCC', 'NCC', 'NCCENR', 'LIBELLE', 'CAN', 'COMPARENT'],skiprows=1)
        df_dep = pd.read_csv('data/v_departement_2023.csv', delimiter=',', header=None, names=['DEP','REG','CHEFLIEU','TNCC','NCC','NCCENR','LIBELLE'],skiprows=1)
        df_reg = pd.read_csv('data/v_region_2023.csv', delimiter=',', header=None, names=['REG','CHEFLIEU','TNCC','NCC','NCCENR','LIBELLE'],skiprows=1)
        df_serie_hist = pd.read_csv('data/base-cc-serie-historique-2020.csv', delimiter=';', header=None, names=['CODGEO', 'P20_POP', 'P14_POP', 'P09_POP', 'D99_POP', 'D90_POP',
       'D82_POP', 'D75_POP', 'D68_POP', 'SUPERF', 'NAIS1420', 'NAIS0914',
       'NAIS9909', 'NAIS9099', 'NAIS8290', 'NAIS7582', 'NAIS6875', 'DECE1420',
       'DECE0914', 'DECE9909', 'DECE9099', 'DECE8290', 'DECE7582', 'DECE6875',
       'P20_LOG', 'P14_LOG', 'P09_LOG', 'D99_LOG', 'D90_LOG', 'D82_LOG',
       'D75_LOG', 'D68_LOG', 'P20_RP', 'P14_RP', 'P09_RP', 'D99_RP', 'D90_RP',
       'D82_RP', 'D75_RP', 'D68_RP', 'P20_RSECOCC', 'P14_RSECOCC',
       'P09_RSECOCC', 'D99_RSECOCC', 'D90_RSECOCC', 'D82_RSECOCC',
       'D75_RSECOCC', 'D68_RSECOCC', 'P20_LOGVAC', 'P14_LOGVAC', 'P09_LOGVAC',
       'D99_LOGVAC', 'D90_LOGVAC', 'D82_LOGVAC', 'D75_LOGVAC', 'D68_LOGVAC',
       'P20_PMEN', 'P14_PMEN', 'P09_PMEN', 'D99_PMEN', 'D90_NPER_RP',
       'D82_NPER_RP', 'D75_NPER_RP', 'D68_NPER_RP'],skiprows=1)
        df_mariage_meta_donnees = pd.read_csv('data/irsocmar2021_dep_csv/metadonnees_irsocmar2021_annuels.csv', delimiter=';', header=None, names=['COD_VAR','LIB_VAR','COD_MOD','LIB_MOD1'],skiprows=1)
        df_mariage_sexe_dep = pd.read_csv('data/irsocmar2021_dep_csv/Dep1.csv', delimiter=';', header=None, names=['TYPMAR3','REGDEP_MAR','GRAGE','NBMARIES'],skiprows=1)
        df_mariage_etat_mat_anterieur = pd.read_csv('data/irsocmar2021_dep_csv/Dep2.csv', delimiter=';', header=None, names=['TYPMAR','REGDEP_MAR','SEXE','ETAMAT','NBMARIES'],skiprows=1)
        df_mariage_sexe_dep_1er_fois = pd.read_csv('data/irsocmar2021_dep_csv/Dep3.csv', delimiter=';', header=None, names=['TYPMAR3','REGDEP_MAR','GRAGE','NBMARIES'],skiprows=1)
        df_nation_dep_domicile_conjugale = pd.read_csv('data/irsocmar2021_dep_csv/Dep4.csv', delimiter=';', header=None, names=['TYPMAR2','REGDEP_DOMI','NATEPOUX','NBMAR'],skiprows=1)
        df_pays_naissance_dep_domicile_conjugale = pd.read_csv('data/irsocmar2021_dep_csv/Dep5.csv', delimiter=';', header=None, names=['TYPMAR2','REGDEP_DOMI','LNEPOUX','NBMAR'],skiprows=1)
        df_repartition_mensuelle = pd.read_csv('data/irsocmar2021_dep_csv/Dep6.csv', delimiter=';', header=None, names=['TYPMAR2','REGDEP_MAR','MMAR','NBMAR'],skiprows=1)
        df_superficie_mayotte = pd.read_csv('data/superficie_Mayotte.csv', delimiter=';', header=None, names=['CODE_COMMUNE','SUPERFICIE'],skiprows=1)
        

        # Supprimer les lignes vides
        #df_serie_hist = df_serie_hist.dropna(how='any')
        
        listCOM = []

        ####################################################################################################
        print("Insertion dans la table Type_Mariage ...")
        print("Insertion dans la table Type_Sexe ...")
        print("Insertion dans la table Type_Etat_matrimoniale ...")
        print("Insertion dans la table Type_Article ...")
        print("Insertion dans la table Type_Mixite_Epoux ...")
        print("Insertion dans la table Mois_Mariage ...")
        
        cursor.execute("""
                       INSERT INTO type_article (CODE_ARTICLE, DESCRIPTION, CHARNIERE) VALUES 
                       (0, 'Pas d''article et le nom commence par une consonne sauf H muet', 'charnière = DE'),
                       (1, 'Pas d''article et le nom commence par une voyelle ou un H muet', 'charnière = D'),
                       (2, 'Article = LE', 'charnière = DU'),
                       (3, 'Article = LA', 'charnière = DE LA'),
                       (4, 'Article = LES', 'charnière = DES'),
                       (5, 'Article = L', 'charnière = DE L'),
                       (6, 'Article = AUX', 'charnière = DES'),
                       (7, 'Article = LAS', 'charnière = DE LAS'),
                       (8, 'Article = LOS', 'charnière = DE LOS')
                       """)
        
        cursor.execute("""
                       INSERT INTO type_annee_population (ANNEE_POPULATION, DESCRIPTION) VALUES
                       (1968, 'Population en 1968'),
                       (1975, 'Population en 1975'),
                       (1982, 'Population en 1982'),
                       (1990, 'Population en 1990'),
                       (1999, 'Population en 1999'),
                       (2009, 'Population en 2009'),
                       (2014, 'Population en 2014'),
                       (2020, 'Population en 2020')
                       """)
        # Parcourir chaque ligne du DataFrame et insérer les données dans la table
        for index, row in df_mariage_meta_donnees.iterrows():
        
            #Insertion dans la table Type_Nationalite_Epoux
            if row['COD_VAR']=="MMAR" and row['COD_MOD']!="AN":
                cursor.execute("INSERT INTO Mois_Mariage (MOIS, DESCRIPTION) VALUES (%s, %s)",
                        (row['COD_MOD'], row['LIB_MOD1']))
                
            #Insertion dans la table Type_Intervalle_Année
            if row['COD_VAR']=="GRAGE" and row['COD_MOD']!="TOTAL":
                if row['COD_MOD'][-2:] == "PL":
                    # Si c'est le cas, utilisez une valeur spécifique pour AGE_FIN
                    age_fin = 200
                else:
                    # Sinon, utilisez la valeur normale de row['GRAGE'][-2:]
                    age_fin = row['COD_MOD'][-2:]

                cursor.execute("INSERT INTO Type_Intervalle_Age (AGE_DEBUT, AGE_FIN, DESCRIPTION) VALUES (%s, %s, %s)",
                        (row['COD_MOD'][:2], age_fin, row['LIB_MOD1']))
                
            #Insertion dans la table Type_Mixite_Epoux
            if row['COD_VAR']=="NATEPOUX" and row['COD_MOD']!="TOTAL":
                cursor.execute("INSERT INTO Type_Mixite_Epoux (MIXITE_EPOUX, DESCRIPTION) VALUES (%s, %s)",
                        (row['COD_MOD'], row['LIB_MOD1']))
            
            #Insertion dans la table Type_Mariage
            if row['COD_VAR']=="TYPMAR2":
                cursor.execute("INSERT INTO Type_Mariage (MARIAGE, DESCRIPTION) VALUES (%s, %s)",
                        (row['COD_MOD'], row['LIB_MOD1']))
                
            #Insertion dans la table Type_Sexe
            if row['COD_VAR']=="SEXE":
                cursor.execute("INSERT INTO Type_Sexe (SEXE, DESCRIPTION) VALUES (%s, %s)",
                        (row['COD_MOD'], row['LIB_MOD1']))
                
            #Insertion dans la table Type_Etat_matrimoniale
            if row['COD_VAR']=="ETAMAT":
                cursor.execute("INSERT INTO Type_Etat_matrimoniale (ETAT_MATRIMONIALE, DESCRIPTION) VALUES (%s, %s)",
                        (row['COD_MOD'], row['LIB_MOD1']))
                

        ####################################################################################################
        print("-----------------------------------")
        print("Insertion dans la table Commune ...")

        # Parcourir chaque ligne du DataFrame et insérer les données dans la table
        for index, row in df_com.iterrows():
            if row['TYPECOM']!="COM":
                continue
            #Insertion dans la table Communes
            listCOM.append(row['COM'])
            cursor.execute("INSERT INTO Commune (CODE_COMMUNE, CODE_COLLECTIVITE_TERRITORIALE, CODE_ARTICLE, NOM_MAJUSCULE, NOM_RICHE, LIBELLE) VALUES (%s, %s, %s, %s, %s, %s)",
                        (row['COM'], row['CTCD'], row['TNCC'], row['NCC'], row['NCCENR'], row['LIBELLE']))
            
        ####################################################################################################
        print("-----------------------------------")
        print("Insertion dans la table Departement ...")
        
        for index, row in df_dep.iterrows():
            #Insertion dans la table Departements
            cursor.execute("INSERT INTO Departement (CODE_DEPARTEMENT,CHEFLIEU,CODE_ARTICLE, NOM_MAJUSCULE, NOM_RICHE, LIBELLE) VALUES (%s, %s, %s, %s, %s, %s)",
                        (row['DEP'], row['CHEFLIEU'], row['TNCC'], row['NCC'], row['NCCENR'], row['LIBELLE']))
        
        ####################################################################################################
        print("-----------------------------------")
        print("Insertion dans la table Region ...")
            
        for index, row in df_reg.iterrows():
            #Insertion dans la table Regions
            cursor.execute("INSERT INTO Region (CODE_REGION,CHEFLIEU,CODE_ARTICLE, NOM_MAJUSCULE, NOM_RICHE, LIBELLE) VALUES (%s, %s, %s, %s, %s, %s)",
                        (row['REG'], row['CHEFLIEU'], row['TNCC'], row['NCC'], row['NCCENR'], row['LIBELLE']))
        
        ####################################################################################################
        print("-----------------------------------")
        print("Insertion dans la table Departement_Region ...")
        
        for index, row in df_dep.iterrows():
        #Insertion dans la table DepReg
            cursor.execute("INSERT INTO Departement_Region (CODE_DEPARTEMENT, CODE_REGION) VALUES (%s, %s)",
                        (row['DEP'],row['REG']))
        
        ####################################################################################################
        print("-----------------------------------")
        print("Ajout de Code_Departement dans la table Commune")

        #Ajout de la collone Dep dans la table Communes
        cursor.execute("ALTER TABLE Commune ADD COLUMN CODE_DEPARTEMENT VARCHAR(3)")
        cursor.execute("ALTER TABLE Commune ADD CONSTRAINT fk_departement FOREIGN KEY (CODE_DEPARTEMENT) REFERENCES departement(CODE_DEPARTEMENT)")

        for index, row in df_com.iterrows():
            if row['TYPECOM']!="COM":
                continue
            
            #Ajout des valeurs Dep dans la table Communes
            cursor.execute("UPDATE Commune SET CODE_DEPARTEMENT = %s WHERE CODE_COMMUNE = %s",
                    (row['DEP'], row['COM']))
        
        ####################################################################################################
        print("-----------------------------------")

        cursor.execute("ALTER TABLE Commune ADD COLUMN Superficie REAL")

        print("Ajout de la superficie des communes de Mayotte dans la table Commune ")
        print("Ajout de la superficie dans la table Commune ")

        for index, row in df_superficie_mayotte.iterrows():
                    #Ajout valeur superficie des communes de Mayotte dans la table Commune
                    cursor.execute("UPDATE Commune SET superficie = %s WHERE CODE_COMMUNE = %s",
                            (row['SUPERFICIE'], str(int(row['CODE_COMMUNE']))))
                    
        print("Insertion dans la table Deces_Commune,Naissance_Commune,Population_Commune ...")

        for index, row in df_serie_hist.iterrows():
            codgeo = str(row['CODGEO']).zfill(5)
            if codgeo not in listCOM:
                continue
            
            #Ajout valeur superficie dans la table Commune
            cursor.execute("UPDATE Commune SET superficie = %s WHERE CODE_COMMUNE = %s",
                    (row['SUPERF'], codgeo))
        
        ###################################################################################################

            #Insertion dans la table Deces_Commune pour l'année 2014-2020
            cursor.execute("INSERT INTO Deces_Commune (CODE_COMMUNE, ANNEE_DEBUT, ANNEE_FIN, NOMBRE) VALUES (%s, %s, %s, %s)",
                        (codgeo,2014,2020,row['DECE1420']))

            #Insertion dans la table Deces_Commune pour l'année 2009-2014
            cursor.execute("INSERT INTO Deces_Commune (CODE_COMMUNE, ANNEE_DEBUT, ANNEE_FIN, NOMBRE) VALUES (%s, %s, %s, %s)",
                        (codgeo,2009,2014,row['DECE0914']))
            
            #Insertion dans la table Deces_Commune pour l'année 1999-2009
            cursor.execute("INSERT INTO Deces_Commune (CODE_COMMUNE, ANNEE_DEBUT, ANNEE_FIN, NOMBRE) VALUES (%s, %s, %s, %s)",
                        (codgeo,1999,2009,row['DECE9909']))

            #Insertion dans la table Deces_Commune pour l'année 1990-1999
            cursor.execute("INSERT INTO Deces_Commune (CODE_COMMUNE, ANNEE_DEBUT, ANNEE_FIN, NOMBRE) VALUES (%s, %s, %s, %s)",
                        (codgeo,1990,1999,row['DECE9099']))
            
            #Insertion dans la table Deces_Commune pour l'année 1982-1990
            cursor.execute("INSERT INTO Deces_Commune (CODE_COMMUNE, ANNEE_DEBUT, ANNEE_FIN, NOMBRE) VALUES (%s, %s, %s, %s)",
                        (codgeo,1982,1990,row['DECE8290']))
            
            #Insertion dans la table Deces_Commune pour l'année 1975-1982
            cursor.execute("INSERT INTO Deces_Commune (CODE_COMMUNE, ANNEE_DEBUT, ANNEE_FIN, NOMBRE) VALUES (%s, %s, %s, %s)",
                        (codgeo,1975,1982,row['DECE7582']))

            #Insertion dans la table Deces_Commune pour l'année 1968-1975
            cursor.execute("INSERT INTO Deces_Commune (CODE_COMMUNE, ANNEE_DEBUT, ANNEE_FIN, NOMBRE) VALUES (%s, %s, %s, %s)",
                        (codgeo,1968,1975,row['DECE6875']))
            
        ####################################################################################################
            
            #Insertion dans la table Naissance_Communes pour l'année 2014-2020
            cursor.execute("INSERT INTO Naissance_Commune (CODE_COMMUNE, ANNEE_DEBUT, ANNEE_FIN, NOMBRE) VALUES (%s, %s, %s, %s)",
                        (codgeo,2014,2020,row['NAIS1420']))

            #Insertion dans la table Naissance_Communes pour l'année 2009-2014
            cursor.execute("INSERT INTO Naissance_Commune (CODE_COMMUNE, ANNEE_DEBUT, ANNEE_FIN, NOMBRE) VALUES (%s, %s, %s, %s)",
                        (codgeo,2009,2014,row['NAIS0914']))
            
            #Insertion dans la table Naissance_Communes pour l'année 1999-2009
            cursor.execute("INSERT INTO Naissance_Commune (CODE_COMMUNE, ANNEE_DEBUT, ANNEE_FIN, NOMBRE) VALUES (%s, %s, %s, %s)",
                        (codgeo,1999,2009,row['NAIS9909']))

            #Insertion dans la table Naissance_Communes pour l'année 1990-1999
            cursor.execute("INSERT INTO Naissance_Commune (CODE_COMMUNE, ANNEE_DEBUT, ANNEE_FIN, NOMBRE) VALUES (%s, %s, %s, %s)",
                        (codgeo,1990,1999,row['NAIS9099']))
            
            #Insertion dans la table Naissance_Communes pour l'année 1982-1990
            cursor.execute("INSERT INTO Naissance_Commune (CODE_COMMUNE, ANNEE_DEBUT, ANNEE_FIN, NOMBRE) VALUES (%s, %s, %s, %s)",
                        (codgeo,1982,1990,row['NAIS8290']))
            
            #Insertion dans la table Naissance_Communes pour l'année 1975-1982
            cursor.execute("INSERT INTO Naissance_Commune (CODE_COMMUNE, ANNEE_DEBUT, ANNEE_FIN, NOMBRE) VALUES (%s, %s, %s, %s)",
                        (codgeo,1975,1982,row['NAIS7582']))

            #Insertion dans la table Naissance_Communes pour l'année 1968-1975
            cursor.execute("INSERT INTO Naissance_Commune (CODE_COMMUNE, ANNEE_DEBUT, ANNEE_FIN, NOMBRE) VALUES (%s, %s, %s, %s)",
                        (codgeo,1968,1975,row['NAIS6875']))
            
            ####################################################################################################
            
            #Insertion dans la table Population_Commune pour l'année 2020
            cursor.execute("INSERT INTO Population_Commune (CODE_COMMUNE, ANNEE, NOMBRE) VALUES (%s, %s, %s)",
                        (codgeo,2020,row['P20_POP']))
            
            #Insertion dans la table Population_Commune pour l'année 2014
            cursor.execute("INSERT INTO Population_Commune (CODE_COMMUNE, ANNEE, NOMBRE) VALUES (%s, %s, %s)",
                        (codgeo,2014,row['P14_POP']))
            
            #Insertion dans la table Population_Commune pour l'année 2009
            cursor.execute("INSERT INTO Population_Commune (CODE_COMMUNE, ANNEE, NOMBRE) VALUES (%s, %s, %s)",
                        (codgeo,2009,row['P09_POP']))
            
            #Insertion dans la table Population_Commune pour l'année 1999
            cursor.execute("INSERT INTO Population_Commune (CODE_COMMUNE, ANNEE, NOMBRE) VALUES (%s, %s, %s)",
                        (codgeo,1999,row['D99_POP']))
            
            #Insertion dans la table Population_Commune pour l'année 1990
            cursor.execute("INSERT INTO Population_Commune (CODE_COMMUNE, ANNEE, NOMBRE) VALUES (%s, %s, %s)",
                        (codgeo,1990,row['D90_POP']))
        
        
        ####################################################################################################
        
        print("-----------------------------------")
        print("Insertion dans la table mariage ...")

        for index, row in df_mariage_sexe_dep.iterrows():
            
                
            if row['GRAGE'] == "TOTAL" or row['REGDEP_MAR']=="FE" or row['REGDEP_MAR'] == "FM" or 'X' in row['REGDEP_MAR']:
                continue

            if row['GRAGE'][-2:] == "PL":
                # Si c'est le cas, utilisez une valeur spécifique pour AGE_FIN
                age_fin = 200
            else:
                # Sinon, utilisez la valeur normale de row['GRAGE'][-2:]
                age_fin = row['GRAGE'][-2:]

            if row['TYPMAR3']=="HF-F" or row['TYPMAR3']=="HF-H":
                typemariage = "HF"
            else:
                typemariage = row['TYPMAR3']

            if len(row['REGDEP_MAR'])==3:
                code_departement = row['REGDEP_MAR']

            if len(row['REGDEP_MAR'])==4:
                code_departement = row['REGDEP_MAR'][-2:]
            
            #Insertion dans la table mariage pour un mariage Homme-Homme
            cursor.execute("INSERT INTO mariage (CODE_DEPARTEMENT,AGE_DEBUT,AGE_FIN,TYPE_MARIAGE,NOMBRE,SEXE) VALUES (%s, %s, %s, %s, %s, %s)",
                        (code_departement, row['GRAGE'][:2], age_fin, typemariage, row['NBMARIES'], row['TYPMAR3'][-1]))

        ####################################################################################################
        print("-----------------------------------")
        print("Insertion dans la table mariage_etat_matrimoniale_anterieur ...")
        
        for index, row in df_mariage_etat_mat_anterieur.iterrows():
                
            if row['REGDEP_MAR']=="FE" or row['REGDEP_MAR'] == "FM" or 'X' in row['REGDEP_MAR']:
                continue
                
            if row['TYPMAR']=="HF":
                type_mariage = row['TYPMAR']
            else:
                type_mariage = row['SEXE']+row['SEXE']

            if len(row['REGDEP_MAR'])==3:
                code_departement = row['REGDEP_MAR']

            if len(row['REGDEP_MAR'])==4:
                code_departement = row['REGDEP_MAR'][-2:]
            
            #Insertion dans la table mariage_etat_matrimoniale_anterieur
            cursor.execute("INSERT INTO mariage_etat_matrimoniale_anterieur (CODE_DEPARTEMENT,ETAT_MATRIMONIALE,NOMBRE,SEXE,TYPE_MARIAGE) VALUES (%s, %s, %s, %s, %s)",
                        (code_departement, row['ETAMAT'], row['NBMARIES'], row['SEXE'], type_mariage))
            
        ####################################################################################################
        print("-----------------------------------")
        print("Ajout de la collone premiere_fois dans la table mariage ...")

        #Ajout de la collone premiere_fois dans la table Communes
        cursor.execute("ALTER TABLE mariage ADD COLUMN premiere_fois INT")

        for index, row in df_mariage_sexe_dep_1er_fois.iterrows():
            
                
            if row['GRAGE'] == "TOTAL" or row['REGDEP_MAR']=="FE" or row['REGDEP_MAR'] == "FM" or 'X' in row['REGDEP_MAR']:
                continue

            if row['GRAGE'][-2:] == "PL":
                # Si c'est le cas, utilisez une valeur spécifique pour AGE_FIN
                age_fin = 200
            else:
                # Sinon, utilisez la valeur normale de row['GRAGE'][-2:]
                age_fin = row['GRAGE'][-2:]

            type_mariage = row['TYPMAR3'][:2]
            sexe = row['TYPMAR3'][-1]

            if len(row['REGDEP_MAR'])==3:
                code_departement = row['REGDEP_MAR']

            if len(row['REGDEP_MAR'])==4:
                code_departement = row['REGDEP_MAR'][-2:]
            
            #Ajout des valeurs premiere_fois dans la table mariage
            cursor.execute("UPDATE mariage SET premiere_fois = %s WHERE CODE_DEPARTEMENT = %s and AGE_DEBUT = %s and AGE_FIN = %s and TYPE_MARIAGE = %s and SEXE = %s",
                            (row['NBMARIES'], code_departement, row['GRAGE'][:2], age_fin, type_mariage, sexe))
                    
        
        ####################################################################################################
        print("-----------------------------------")
        print("Insertion dans la table mariage_nationalite_pays_epoux ...")
        
        for index, row in df_nation_dep_domicile_conjugale.iterrows():
                
            if row['NATEPOUX']=="TOTAL" or row['REGDEP_DOMI']=="FE" or row['REGDEP_DOMI'] == "FM" or 'X' in row['REGDEP_DOMI']:
                continue
                
            if len(row['REGDEP_DOMI'])==3:
                code_departement = row['REGDEP_DOMI']

            if len(row['REGDEP_DOMI'])==4:
                code_departement = row['REGDEP_DOMI'][-2:]

            
            #Insertion dans la table mariage_nationalite_pays_epoux
            cursor.execute("INSERT INTO mariage_nationalite_pays_epoux (CODE_DEPARTEMENT,TYPE_MARIAGE,MIXITE_EPOUX,NOMBRE_NATIONALITE) VALUES (%s, %s, %s, %s)",
                        (code_departement, row['TYPMAR2'], row['NATEPOUX'], row['NBMAR']))
        
        ####################################################################################################
        print("-----------------------------------")
        print("Ajout de la collone premiere_fois dans la table mariage_nationalite_pays_epoux ...")


        for index, row in df_pays_naissance_dep_domicile_conjugale.iterrows():
            
                
            if row['LNEPOUX']=="TOTAL" or row['REGDEP_DOMI']=="FE" or row['REGDEP_DOMI'] == "FM" or 'X' in row['REGDEP_DOMI']:
                continue
                
            if len(row['REGDEP_DOMI'])==3:
                code_departement = row['REGDEP_DOMI']

            if len(row['REGDEP_DOMI'])==4:
                code_departement = row['REGDEP_DOMI'][-2:]
            
            #Ajout des valeurs premiere_fois dans la table mariage_nationalite_pays_epoux
            cursor.execute("UPDATE mariage_nationalite_pays_epoux SET NOMBRE_PAYS_NAISSANCE = %s WHERE CODE_DEPARTEMENT = %s and TYPE_MARIAGE = %s and MIXITE_EPOUX = %s",
                            (row['NBMAR'], code_departement, row['TYPMAR2'], row['LNEPOUX']))

        ####################################################################################################
        print("-----------------------------------")
        print("Insertion dans la table mariage_repartition_mensuelle ...")
        
        for index, row in df_repartition_mensuelle.iterrows():
                
            if row['MMAR']=="AN" or row['REGDEP_MAR']=="FE" or row['REGDEP_MAR'] == "FM" or 'X' in row['REGDEP_MAR']:
                continue
                
            if len(row['REGDEP_MAR'])==3:
                code_departement = row['REGDEP_MAR']

            if len(row['REGDEP_MAR'])==4:
                code_departement = row['REGDEP_MAR'][-2:]

            
            #Insertion dans la table mariage_repartition_mensuelle
            cursor.execute("INSERT INTO mariage_repartition_mensuelle (CODE_DEPARTEMENT,TYPE_MARIAGE,MOIS,NOMBRE) VALUES (%s, %s, %s, %s)",
                        (code_departement, row['TYPMAR2'], row['MMAR'], row['NBMAR']))

        ####################################################################################################
                
        # Valider la transaction
        conn.commit()
                         
        
        conn.commit()
        print("Données importées avec succès.")
    
    except Exception as e:
        print(f"Erreur lors de l'importation des données: {e}")

    # Fermeture de la connexion à la base de données
    conn.close()
    print("Connexion à la base de données PostgreSQL fermée.")